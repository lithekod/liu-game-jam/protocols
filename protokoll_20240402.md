# Game Jam möte 2024-04-02
- Mötet öppnas 17:46

- Närvarande
    - Martin
    - Johannes
    - Fabian
    - Henry
    - Filip
    - Elias

## Status
- Datum spikat
- Lokal fixad
- Posters och stickers på väg
- Budget bör inte vara något problem

## Nyckeluthämtning
- Martin inväntar svar om nyckeluthämtning
- Förutspår att det går att hämta ut på torsdagen eller fredagen
- Johannes hämtar ut nycklarna

## Inköp
- Handla vid coop Riddarhyttan (?) istället för att hyra bil och allt sånt
- Budget
    - ungefär samma som förra gången, lite mer för snacks
    - Martin kollar upp
- Köpa samma saker som inför förra jam:et
    - frukost
    - frukt
    - lite snacks till gemensamma moment (presentationer)
- Inköpslista skrivs av Martin inför fredagen
- Vilka handlar
    - Martin
    - Emanuel troligtvis
    - Fabian

## Saker från förrådet
- Vilka bär
    - Filip
    - Elias 
    - Henry
- Samlas vid B-huset kl. 15. Henry låser upp förrådet
- Det som ska tas med
    - Grenkontakter (10 st)
    - Salta pinnar
    - Banners (2 st)
    - Skålar
    - Pennor & papper
    - Pizzaslicers
    - Te (1 låda)
    - Stickers
    - Kodapan (optional)

## Lokalen
- Vilka organisers kan vara på plats
- Bra om organisers är där som senast vid kl. 16.30
    - Var gärna där så tidigt som möjligt
    - Inte tidigare än Johannes som hämtar ut nycklarna såklart :P

## Matbeställning
- Pizza på fredagen
    - Deadline kl. 18.30
    - Anmälan i incheckningsformuläret som vanligt
- Sushi på lördag kväll
    - Formulär på samma sätt som pizzan 
    - Deadline till kl. 12 (?)
- Fabian vill inte ligga ute med en stor mängd pengar i en längre period
    - 1-2 veckor är okej men inte en hel månad 
        - (Budget som student liksom. Låt oss leva)
    - Preliminärt är Emanuel på plats och kan fixa pengarna etc direkt
    - I annat fall kan Martin ligge ute med pengarna

## Uppgiftsfördelning
- Ta emot folk och uppdatera incheckningsformuläret
    - Johannes, Fabian 
- Hålla i presentationer
    - Henry
- Matbeställning
    - Fabian
- Hålla i temagenerering
    - Elias
- Ställa fram frukt, frukost och snacks
    - Filip

## Rollfördelning recap.
- Marknadsföringsansvarig (ej on-site): Elias
- Presentationsansvarig: Henry
- Städansvarig: Johannes
- Matansvarig: Fabian
- Frukostansvarig: Filip

## Övrigt
- Rekrytera nytt folk
    - Johannes och Martin trycker på det vid incheckning
        - Lägga till möjlighet att uttrycka intresse för att bli organiser i incheckningsformuläret
    - Martin kommer gå runt och rizz:a upp folk under jam:et
    - Henry trycker på det vid presentationerna

## Nästa möte
- Feedback-möte
- När: Måndag 2024-04-22 kl. 17:45

## Mötet avslutas
- Kl. 18:20
    - 45 min möte sheeeeeesh
    - Game Jam möte any% speedrun wr ???! :O
