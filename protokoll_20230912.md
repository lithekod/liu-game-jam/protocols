# Mötesprotokoll Game Jam
## 12 september 2023

## Kallade
- Organisers som anmält sig för workshop/föreläsning

## Närvarande
- Martin
- Elias
- Gustav
- Annie
- Jesper
- Johannes

## Mötets öppnades 17:35

## Marknadsföring på föreläsningar
- Marknadsföra på masterkurser
	- Inte så bra idé
	- Vill isf ha att föreläsaren talar om game jam, annars lär eleverna springa iväg

- Fokusera på nyare elever
	- Kan isf ej zooma in på de som har erfarenhet/intresse av spelprogrammering

- Ingemar
	- Annie har sagt delgivit våra planera om marknadsföring
	- Han kommer troligtvis göra reklam åt oss
	- Ingemar tycker det är en bra idé att vi kontaktar andra examinatorer med ett datum
	- Kursen Annie har med Ingemar har inga föreläsningar i början på period 2 
	- Vi bör kontakta honom snart

- Ha en slide som föreläsare kan ha med i deras föreläsningar
	- ?

- Kan ge en poster till föreläsare
	- Kan ligga framme i pausen
	- Folk får något visuellt att koppla game jam till
	- Folk kan lättare gå fram och läsa detaljer

- Vilka kan hålla i marknadsföring på föreläsningar
	- Martin & Elias

## Andra kanaler för marknadsföring
- LiU Gamers
	- Be folk speltesta
	- Skapa ett meddelande med info om Game Jam och be om att få lägga ut i ex. deras Discord-server
	- Måste se till att ha tillåtelse annars dålig stil

## Workshop planering
### Hur många workshops
- Samma workshop på flera tillfällen?
- Flera workshops över flera tillfällen?
- Hålla föreläsning i Ada under meet up för LitheKod
- Se till att inte hålla i olika workshops under samma tid 

- Samma sak flera gånger är rimligast
- I framtiden ha en workshop per år istället för en per jam
	- Blir mindre repetitivt
	- Fyller den tidiga hösten som annars är tom
	- Blir ett trevligt redskap för LiTHe Kod på sikt (kan lägga in under nolle-p)

### Innehåll och upplägg 
- Populärt att folk sitter och absorberar 
- Om folk ska följa med på egna datorer kommer det vara en massa installationsproblem
- Ge tillgång till kodskelett som folk kan fortsätta bygga på under jam:et

- Ha en grund att utgå ifrån, ex. en enkel platformer
- Utöka spelet tillsammans med publiken
	- Ex ändra smådetaljer
- Behöver inte bli ett färdigt spel

- En workshop som fokuserar på 2D och en som fokuserar på 3D 
- Annie kan gå igenom om att göra grafik och konst för jams
	- Huvudområden: färgteori, artstyle & verktyg/metoder för att skapa art
- Skulle behöva en föreläsningsdel och en del för att göra spel

- Göra upp en tidsbudget per workshop (ex. 2h/workshop)
	- Lättare för de som håller i workshops att lägga upp deras delar om de vet hur mycket tid de får på sig 
	-

- Inlärning är inte huvudmålet utan mer visa att det går att göra ett spel
	- Lite annorlunda för grafik för att många är rädda och ser det som svårt (grafikdel behöver fokus på inlärning)
	- Kanske typ 10 min teori och resten av tiden visa att det går att skapa saker

- Se till så att fokuset inte bara är på programmering
	- Har tidigare saknats genomgång av ex. grafik

### Lunchföreläsningsserie
- I en vecka i anslutning till jam:et
- Idéskapning
- Hur programmerar man 2D-spel
- Hur programmerar man 3D-spel
- Hur gör man grafik
- Gå igenom del för del från grund och vidare
- Kan vara svårt att få folk att gå på dem eftersom man behöver gå på alla
- Måste göras fristående vilket kan vara svårt
- Skulle kunna lova mat men det kanske ej finns budget för det
- Se till att stå i Coloseum tillräckligt tidigt så att vi inte måste göra separat reklam för workshops
- Se till så att all reklam för workshops sker tillsammans med reklam för jam:et
	- Marknadsföra som att "Under 1 timme kan du lära dig skapa spel. Det är inte svårt. Kom på game jam :)"

### Summa sumarum
- Workshop med olika delar
- Lite teori, fokus på att visa att det går att göra spel
- Inte att folk följer och gör samma på deras datorer
- 1 workshop under på två tillfällen
- Den första som börjar jobba på workshop slides gör en presentation sparad i Google Drive 
- Typ deadline om 4 veckor

### Konkret vilka delar (+ tidsbudget)
- Speldesign 15 min med Gustav
	- vad att tänka på för när man designar för ett game jam
	- Ha "regler" som vi utgår ifrån när vi designar spel
	- Gemensam brainstorm 
	- Avsluta med idén som vi ska jobba vidare på
- Art 15 min med Annie 
	- budskap "var inte rädd för art"
- Spelprogrammering (teknisk del) i Godot resten av tiden med Elias
- Liten uppvisning av Löve2D på slutet med Gustav

- Publikinteraktion i form av illusion av val (railroad) eller fördefinierade val

### Övrigt
- Jesper tar över ansvaret att göra posters

### Nästa möte relaterat till workshop
- vecka 41 eller 42
