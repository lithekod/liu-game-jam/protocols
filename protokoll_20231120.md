# Feedbackmöte efter Game Jam 2023-11-20 
## Mötets öppnande 17:46

## Närvarande
Martin
Elias
Fabian
Gustav
Annie
Johannes

## Delegering av arbete och struktur
- Martin behövde ha koll på allt som behövde göras vilket inte trevligt för honom
- Behöver mer struktur
	- Roller
		- Roll för frukostansvarig fungerade bra
		- Ha fler roller
		- Definera de roller som finns tydligare
		- Ansvarig för game jam ser till att påminna folk
	- Ha någon form av dagsordning, vad som behövs göra och när
		- Dela ut efter dagsordningen i förväg
		- Testades förut men fungerade inte väl för att folk glömde göra deras uppgifter
		- Inget som går att åstadkomma väl på bara ett game jam 

- Hur implementera roller
	- Först definera rollerna tydligt
	- Därefter delegera ut roller

- Processdokument på olika nivåer
	- Detaljnivå för de som ej varit med förut
	- Mellannivå
	- Högsta nivå som är översiktlig, typ en punktlista
	- Dokument behöver uppdateras vilket ofta inte blir gjort

- Faddersystem
	- Ha en fadder per organiser
	- Organisers frågor sin fadder när de stöter på oklarheter

## Planering
- Gustav tänker undersöka eventplaneringssystem 
- Använda organiserkalendern mer
	- Sikta på att organisers kollar på kalendern för att sa vad som behöver göras

## Mat
- Sluta köpa apelsiner för folk äter knappt av dem

## Discord för föreningar
- Det är svårt att kommunicera med andra föreningar vilket leder till krockar med evenemang
- Göra en discord för att lösa detta
- Bjud in föreningar inom samma område, exempelvis lan, hackaton etc
- Ha hela styret för varje utskott/förening
	- Öka chansen att folk i en förening ser meddelanden
	- Mer öppen konversation

## Ta inte med lika mycket från förrådet
- Det blev många saker från förrådet som vi inte använde
- Sluta planera att ta med oss sådana saker
	- Pennor finns redan i Spektrum
	- Behövs inte en massa papper
	- Behövs inte så många grenkontakter
- Det blir väldigt tugnt att bära och ej optimalt att någon måste ta hem saker från förrådet hem
	- Har hittills bara fungerat för att Emanuel bor nära 
	- Inte hållbar lösning

## Flytta fram slutpresentationen
- Få mer tid att städa
	- Inte kul att komma hem vid kl 22 på kvällen
- Flytta fram med 1-1.5 timmar

## Städning
- Behöver mer struktur
- Vi hade en punktlista att följa men det blev ändå oorganiserat
	- Punktlistan behöver förbättras
- Sätta en klar tidpunkt för när (i princip) alla ska städa tillsammans
	- Alla samlas och syncas
- Se till att folk inte väntar på nästa städuppgift när de är klara med sin nuvarande

## Punkter från Gustav som vi kan använda nästa gång
- Formulär
	- Testa dem innan
	- Skicka ut länk även på discord istället för bara via mail
		- En del personer fick mailen väldigt sent
		- Vi vill å andra sidan inte att folk som ej deltar fyller i via discord-länk
		- Kanske best att skicka länkar via mail men även skriva att discordmeddelande om att mail är utskickade

## Tidigare Fall Game Jam
- Vi har inte haft game jams så här sent förut
- Vi vill ha fall game jam någon gång mitt i terminen, ungefär precis efter tenta-p
- För tidigt att ha det innan tenta-p?
	- Kan ha det i mitten av period 1
	- Behöver att vi börjar planera redan under sommaren
	- Fördel är att folk är mindre skoltrötta

## Workshops
- Flera personer som var på workshops dök upp på jam:et
	- 50% av förstagångsdeltagarna var på workshoppen
	- Många av dem hörde om workshoppen via discord
- Flera tyckte att workshop var bra
	- Särksilt delen om Art uppskattades
- Troligen relevant ha workshops igen
	- Inte inför varje game jam
	- På hösten får vi nya elever som inte har mycket erfarenhet med bl.a. programmering
- Arbetsmängd
	- Nu när slides:en är gjorda finns en grund som förenklar att hålla i workshops igen
	- Inte mycket arbete för delen om brainstorming och delen för godot
- Vi kan troligen dra in många personer med workshops om marknadsför bättre och håller i dem vid rätt tid
- Överlag inte så stor börda och var kul att hålla i

## Posters
- Endast 2 personer hörde om Game jam via posters
- Kanske ändra hur vi sätter upp posters

## Budgetcheck
- 11 600kr utan posters medräknade
	- Spektrum kostar lite
	- Kanske dra ner på antalet posters

## Fördelning av arbete innan game jams
- Vi skriver ner vad som behöver göras på möten men kollar sällan på protokollen
- Behöver att vi kollar igenom protokollen och faktiskt får saker gjorda
- Delegera vilka som gör vad och när
	- Den som håller i mötet ser till att stå för detta
	- Göra detta under mötet

## Scaled back global game jam
- Skala ner pga timing och organiser effort
- Boka Spektrum (eller annan lokal)
	- Spektrum är kanske för mycket för vad som krävs för tillbakaskalat
	- Å andra sidan inte lika trevligt att spendera en hel helg på campus
	- GoTo10 uppbokad av Hackaton
	- Spektrum blir troligen bästa alternativet
- Ha presentation för intro & outro
- Kanske ha playtesting
- Ingen marknadsföring utöver lite discord announcements
- Tar bort hantering av mat vilket är vad som kräver mest arbetsinsats
- Flera av oss organisers kanske inte närvarar
	- Martin har spelning
	- Martin hör av sig till Frans för att se hur mycket Frans kan göra
	- Martin kollar vilka organisers som kan vara där utan risk för avbokning

## Nya organisers
- Lyckades vi få några?
	- en person skrev till Martin
	- det verkade som att en till var intresserad, Martin vet vem
	- Ha en diskussion med dem innan årets slut så att det inte bara faller igenom
- Organisers borde approacha och fråga folk om de är intresserade istället för tvärtom
	- Mindre läskigt för deltagare än att approacha en grupp av organisers
	- Bättre att approacha de som verkar passa/vara intresserade
	- Annie gjorde detta tidigare under distansläget och det drog med fler folk

## Övrigt
- Lithe kod kan inte göra sig av med oss ännu
	- Det fanns folk som hörde om jam:et genom Lithe kod
- Höra av oss i princip nu till GoTo10 inför Spring Game Jam
	- Vi vill veta hur tidsramen ser ut för bokning

## Vem gör vad härefter
- Uppföljning på allt till slutet av veckan, fast inget behöver vara klart tills det
- Definition av roller: Martin
- Processdokument: Gustav
	- Inte helt bestämt om vi vill ha processdokument så Gustav gör lite och vi diskuterar senare
- Faddersystem:  
	- Inget särskilt som behöver fixas, bara att göra inför nästa jam
	- Jesper mentor för nästa person som ger mailutskick
- Eventplaneringssystem: Gustav
- Sluta köpa apelsiner
	- Bestämt
- Discord för föreningar: Martin
- Ta inte med lika mycket från förrådet
	- Ta bara med vad som verkligen behövs
	- Plocka ur saker som inte behövs från påsarna (look at you, piles of paper)
- Flytta fram slutpresentation
	- Beslut att flytta fram det med 1 timme
- Strukturering av städning:
	- Borde uppnås genom rollfördelning & processdokument
- Tidigare fall game jam:
	- Skriva ner till nästa ansvarig inför nästa år (i processdokument ;) )
- Workshop inför spring game jam: Martin
	- lägg påminnelse i kalendern
- Sätt upp färre men större posters: Elias
	- ta upp det helgen efter global game jam
- Fördelning av arbete inför game jams:
	- Uppnås via rollfördelning & processdokument
- Scaled back global global: Martin
	- Kolla hur mycket Frans vill styra
	- Kolla vilka organisers kan delta
- Nya organisers: Martin
- Höra av oss till GoTo10 inför Spring Game Jam: Martin

