### REVIEW MEETING 2025-02-10 ###

Notes by Filip Berg.

Welcome to Morgan, our new organizer!!!

Meeting start at 17:50-ish



## REVIEW OF WINTER GAME JAM 2025 ##


# Organizer thoughts #

We missed a part in cleaning, we were supposed to start dishwashers when starting to clean and empty them before we leave but we started them right before leaving.

Banchan was nice, much better compared to buffé.


# Feedback from Spektrum #

None yet.


# New organizers #

Morgan, thank you and welcome.


# Feedback from Jammers #

2!!! big jammers interested in organizing!




## BUDGET CHECK ##

We have ~15 000 kr left, Winter 2025 Jam cost about ~6 300.

Looks very good.




## FORM RESPONSES ##

4 responses, everyone loves it. Someone wanted lunch on Saturday (not happening).




## NEXT JAM ##


# Date #

Something else is April 11-13.

Spring Game Jam 2025 preliminary date: ........ April 4-6!!!!


# Location #

At Spektrum very probably


# Posters #

A4 posters


# Colosseum #

The week before and the week it happens (but not if it is so maybe not the week before )




## FOOD TO BUY ##

We bought less snacks than last time, it ran out but no one complained in the responses. We think we bought 6 bags, maybe we will buy 10 this time?




## ORGANIZER AVAILABILITY ##

Anyone knows they cannot? no




## FOOD THINGS ##

Same as last time (pizza on Friday evening, sushi for Saturday evening, Banchan as Sunday lunch)

Pizza cost ~1000
Banchan ~400
Sushi ~400




## MARKETING ##

Discord (Our own, LiThe Kod, East Sweden Game?, Liu Gamers?)

Posters

Emails

Facebook post

Posters at Valla folkhögskola?

Colloseum

Ingemar?

Erik Berglund? (email)

Jonas Kvarnström? (email)




## NEXT MEETING ##

nice to have ~2 weeks before

19 Mars 2025




## ORGANIZER ROLES ##

Head Organizer - Martin

Marketing - Elias

Presenter - Beatrice

Food - Johannes

Breakfast - preliminarily Elias, Chen if he wants it???

Cleaning - Morgan

Final cleaning (and delivering stuff back) - Martin, Morgan, Filip




## OTHER STUFF ##

anything from last meeting? no

anything else? no (yes (will Martin jam? (yes)))



## MEETING OVER ##

at 18:32
