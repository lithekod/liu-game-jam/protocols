### WELCOME ###

Start at 17:50 not everyone is here



### Notes ###

Taken by Filip Berg.



### Keys ###

Have to be fetched before 15:00 as Spektrum personell will be away for the weekend.



### Shopping ###

No car needed, will shop at local coop.

Martin will shop, along with Chen and Filip start 15:00 at Coop Lägerhyddan.

On the list: snacks, breakfast, fruit. Full list by Martin.



### To bring from storage ###

10 extension coords, both banners, bowls, pen and paper, pizza slicers (are these gone? are there scissors?), tea, stickers, code monkey.

People who will bring stuff: Gustav and Elias



### Start ###

Last start was 16:00, was more than enough time.



### Who can be there? (friday) ###

at 15:00
not johannes at all! so sad



### Food ###

Pizza on friday order deadline 18:30 - costs the same as last time for attendees
Saturday banchan for dinner order deadline Saturday 12:00 (I think it was) - costs 110 for attendees
Sunday sushi for lunch - costs the same as last time for attendees

Costs have been set



### Next meeting ###

After the jam, so maybe Monday February 10th 17:45



### Anything else? ###

advertising? no colloseum, will do posters



### Meeting over ###

at 18:25

