mötet öppnas 17.47

närvarande

- fabian
- filip
- gustav
- henry
- johannes
- martin

## från förra mötet

- vilka gör vad innan jammet
- vilka organizers kan vara med och när
- vad ska vi ta med från förrådet
- gå igenom loggböcker
- utvärdera planeringsmötesmallen

## vilka gör vad innan jammet

- hur får vi dit saker från förrådet?
  - gustav och johannes
    - och elias? martin frågar
  - banners
  - förlängningssladdar
    - kanske inte alla, 10st borde räcka
  - skålar
  - pizzaskärare
  - tema-grejer
  - tröjor
  - stickers
  - termos
  - kaffe och telådan
- vilka kan preppa på plats innan?
  - vem tar emot direkt vid entren? vi har inte så mycket tid innan (eller har vi?)
  - vi har tillgång från 16.30. innan dess är det inte så viktigt (förutom att handla)
  - står att vi börjar 18 på hemsidan, så ta intropresentationen då
  - någon tar emot vid dörren
- vilka handlar frukost innan första frukosten? (frukt?)
  - frukost innehåller frukost?
      - folk gillar frukt. gå till coop på fredagen
  - filip är fortfarande frukostansvarig
  - vilka handlar
    - filip och fabian
      - och martin
      - muskler starka som trästubbar
    - möts vid coop kl 16 (prick!!)
      - "okej, pog" -marin
    - dom vill ha en lista
      - klart dom vill
      - martin fixar
        - klart han gör

## vilka organizers kan vara med och när

- organizers + frans (kanske annie) (victor fredag)
- "juste jag kollade ju efter det. omg" -marin

## gå igenom ansvarigas loggböcker

- martin har inte gjort något
- ingen har gjort något
- behöver sätta upp något i förväg, typ gemensamt dokument eller något

## utvärdera mötesplaneringsmallen

- vilkendå sa du?
- oklarhet: hög
  - "ironic" -palpatine
- vi verkar inte ha något att utvärdera

## övrigt

- martin gör en budget ikväll, imorgon eller när han har tid
  - och inköpslista
- throwback till när henry åt jalapeno poppers hela helgen
  - diarre for days
  - "okej, pog" -marin
- nya organizer-tröjor har kommit
- kaffe och vattenkokare
  - svårt att frakta
  - cykelkärra? nej?
  - martin tar med kaffe och vattenkokare
  - fabian tar med kaffe och vattenkokare?
- "kan du spela det här McDonalds-ljudet? du vet, dududu, diririr" -henry
  - efter mötet
- hur många tror vi kommer
  - ingen aning
  - några stycken
  - inte jättemånga
  - [X] all of the above
- "det är väldigt enkelt att göra en hemsida. liksom, gör ett spel och lägg upp
  det" -johannes
- gustav har inte gjort presentationer än
  - try not to cry
  - cry a lot
- johannes tänkte planera städningen _under_ jammet istället.
  - det kommer vara annorlunda än spektrum så konstigt att planera innan
  - gå igenom fredag kväll eller lördag fm

mötet avslutas 18.20
