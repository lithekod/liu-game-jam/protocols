# Planeringsmöte inför Global Game Jam 2024

Mötet öppnas 17:49 den 6 december 2023.

## Närvarande

Fabian
Filip
Frans
Gustav
Johannes
Martin

## Mötessekreterare

Gustav

## Hej Filip

Hej

## Global Game Jam 2024

### Datum

GGJ-veckan inkluderar helgen 26-28 januari men det tycker vi är för nära inpå
tentaperioden. Dessutom kan varken Martin eller Fabian den helgen. Ytterligare
har Webb-utskottet planerat ett Hackathon helgen i fråga.

Vi beslutar att hålla jammet en eller två veckor efter helgen 26-28 januari,
beroende på hur tillgängliga lokalerna är.

### Lokal

Vi vill testa att vara på Goto10. Martin frågar om dom kan någon av helgerna i
fråga. I andra hand vill vi vara på Spektrum.

### Posters

Nej

### Colosseum

Inte så mycket effort jämförelsevis. Försök stå vid två tillfällen: en gång
samma vecka som jammet och en gång veckan innan. Martin bokar detta när vi
bestämt ett datum för jammet.

### Matinköp

Vi vill undvika att hyra bil av ekonomiska anledningar. (Och stress.) Vi
skippar snacks (chips och läsk). Frukost är fortfarande trevligt men det går att
ta i ryggsäcken på en cykel.

### Organizer-tillgänglighet

Fråga om dom två helgerna vi väljer mellan för att se om någon av dom inte
skulle gå.

### Gemensam mat

Bara pizza på fredagen.

### Marknadsföring

Vi reflekterade mycket förra gången men det här är fel jam att experimentera.

- Discord är gratis och enkelt.
- Posters är dyra och vi brukar inte ha på Global ändå.
- Colosseum: ja.
- Mail: ja (överför från Jesper).
- Facebook: skapa event.
- Sektioner: fråga några infoutskott.

### Nästa planeringsmöte

Första veckan i läsperioden. Martin kommer skicka en Discord-enkät där vi röstar
med emojisar.

### Vem gör vad

Martin skrev ett rolldokument (finns på Drive:en) som vi utgår ifrån.

- Huvudansvarig: Martin
- Marknadsföring: Elias? Martin frågar.
- Frukost: Filip
- Städ: Johannes
- Mat: Fabian (om det blir rätt helg)
- Presentation: Gustav

Alla ansvariga uppmanas föra någon typ av loggbok över vad dom gör så vi kan
använda som grund till processdokument.

## Från förra mötet

Martin skrev ett rolldokument. Verkade fungera bra.

Gustav kom fram till att vi vill ha processdokument i Drive, men som mindre
dokument så att dom kan bli mer riktade. Dokumenten kan istället innehålla
länkar till andra dokument (inklusive länkar till andra dokuments rubriker, jmf
Wikipedia).

Martin är Filips fadder. Fråga om något är oklart!

Martin eller Elias petar på Jesper om att ta över Mailchimp.

## Nästa möte

Någon gång första veckan i nästa läsperiod.

- Vilka gör saker innan jammet (handla, nycklar).
- Vilka organizers (inkl. on-site) kan det (förhoppningsvis) spikade datumet?
- Bestäm vad vi tar med från förrådet.
- Gå igenom ansvarigas loggböcker.
- Utvärdera planeringsmötesmall.

## Mötet avslutas

18:36
