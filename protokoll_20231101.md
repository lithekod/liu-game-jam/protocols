# Mötesprotokoll Game Jam

## 1 november 2023

## Mötets öppnande

17:50

## Närvarande

-   Martin
-   Fabian
-   Johannes

## Matinköp

-   Behöver sänka vad vi köper in
    -   Lägre budget än förra gången
    -   Det blev ganska mycket över förra gången
    -   Det blev specifikt mycket frukostgrejer över förra gången
-   Göra en provisorisk plan
    -   Uppskatta att det kommer ca 30 personer
    -   Om det kommer fler än förväntat är det troligen lugnt att handla in mer vid behov
    -   Använd kalkyldokumentet för inköpslista
-   Martin, Emanuel och Fabian handlar fredag 17 november
    -   Boka bil via OkQ8 så att bil är tillgänglig till kl 13:00 på fredagen
-   Kolla hur mycket läsk vi har
    -   Det blev mycket läsk över förra gången
    -   Läsk håller sig så det skadar inte att köpa för mycket och spara överskott
    -   Spara mer av läsken som blir över

## Uthämtning av nycklar

-   Johannes hämtar nycklar till Spektrum
-   Anteckna klagomålen de har från förra Game Jam
-   Martin hör av sig till Spektrum och senare hör av sig till Johannes

## Incheckning

-   Någon person som sitter vid disken och ser till att folk checkar in med incheckningsformuläret
-   Låta besökarna fylla i pizzaformuläret i samma veva
-   Kola om incheckningsformuläret behöver uppdateras

## Pizzabeställning

-   Begränsa pizzaalternativen som erbjuds
-   Deadline för pizzabeställning 18.30 på fredagen

## Priser

-   Game Jam har fått lägre budget så priser kan behövas ändras
-   Höja kostnaderna för all mat med 10 kr
-   Nya QR-koder behövs
    -   Oklart vem som har koll på QR-koder för Swish
    -   Martin frågar Emanuel

## Kina-buffé & sushibeställning

-   Göra likadant som förra gången
    -   Dvs Kinabuffé från Kina Thai Ryd med beställningsdeadline 16:00
    -   Sushi från Kina Thai Tornby med beställningsdeadline 12:00
    -   Ta med engångstallrikar + bestick
-   Beställa mindre mängd mat per person
-   Tänk över och ev. minska mängden vegetarisk mat
    -   Blev stor mängd vegetarisk mat förra Game Jam
    -   Kolla hur många som önskar vegetariskt (formulär)

## Omorganisering av arbetsuppdelning

-   Ge arrangörer specifika roller
    -   Frukostansvarig: Fabian
    -   Presentationsansvarig: Martin
    -   Städansvarig: Johannes
    -   (Marknadsansvarig inför Game Jams)

## Övrigt

-   Oklart hur vi transporterar överbliven läsk från Spektrum till förrådet på campus
    -   Kommer vi in i förrådet på en söndag?
    -   Frakta med bil?
    -   Martin kollar med Simon om han kan transportera läsk med sin cykelkärra
    -   Martin kollar med Emanuel om han har tillgång till bil eller kan förvara läsk så länge
-   Genomgång för nya organisers
    -   Var saker finns
    -   Vad man kan hjälpa till med
-   Bra om organisers är på plats omkring kl. 16-17 för att hjälpa till med att bära in mat, sätta upp incheckning etc

## Mötets avslutande

18:30

## Nästa möte

Utvärderingsmöte veckan efter Game Jam, måndag 20 november kl. 17:45
