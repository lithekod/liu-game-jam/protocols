Återkopplingmöte Winter Gamejam 2024:

Närvarande:
Fabian, Elias, Filip, Gustav, Johannes, Martin

Vad tycker vi om lokalen?
Flesta tycker det var bra. Jobbigt inga bestick, men vi kan fixa det själva. Annars mycket trevligare. Lätt att gå och se vad alla gör. Mycket lättare att städa.

Begränsade tider?
Standard för evangemang. De gillar inte när det är över helger. Person som skulle ge ut nyckel var sjuk. Ska kunna gå att ha alla tider i framtiden bara vi sköter oss.

Ingen thaibuffe och sushi
Skönt att inte behöva fixa ihop grejer. Gjorde arbetet mycket lättare. Lite tråkigt att inte ha gemenskapen att käka ihop. Kan behövas mer köksgrejer då inte mycket finns på goto10.
Kan gå att ha något annat än buffe bara då sushi inte är jätte ansträngande. Kan lägga sushi vid halftime för att ha lite bättre gemenskap och blir lättare än att ha buffe.
Det har varit problem tidigare år med sushi, det kan vara enklare att inte ha risken utan lämna matbeställningar öppet.
Finns delade åsikter kring hur det ska göra med mat i framtiden.

Delegering av arbete under jammet.
De flesta kände att det var en rimlig mängd arbetsbelastning. Lite rörigt första kvällen, skulle behövas en checklist så allt blir gjort och folk vet hur de ska göra det.
Städet var lite rörigt men inte så svårt. Blev lite mycket skräp att frakta sista dagen.

Struktur Förbättringar
Måste fixas mer dokumentation och checklistor till nästa jam. Framför allt fredag kväll och söndag städ. Checklistor är väldigt bra. Att ha en demo sluttid vore bra. Martin fixar.

Discord för föreningar
Kontaktat föreningar för att undvika skapa system för att undvik krock

Ta med mindre från förrådet
Kanske för många förgreningar. För få muggar, 3-4 rör behövs. Kanske be folk återanvända muggar. Mängden mat var nog lämplig.

Slutpresentation vid 15
Kändes för tidigt för flera. Var däremot skönt att ta det lugnt efter jammet. Kommer sannolikt skjuta back det lite, men bör lyssna på vad jammers tycker.

Budget check
Inte kollats i detalj ännu. Tröjor: 6100, Pizza: 1200 frukost: 1500 (väldigt estimerat)
Budgeten ligger bra till, borde kunna klara ett jam till med plus.

Tröjor
Mycket pengar att alla organisatörer ska ha 2 tröjor. Kan ha armband? Nametag? Återanvända tröjor? Låna ut tröjor under jam?
Finns delade meningar om orangeatröjor bör ges eller köpas av gamla organizers.


Innan game jam planering

Marknadsföring
Köra färre och mindre posters. Några större till colosseum och jam. Stickers är ett måste. Ha bara lite i marknadsföring.
Design av posters måste bli bättre.

Nästa jam
Datumförslag: 12-14 april, långt från liu gamers, påsk och tentor
Lokal: Prelem. Goto10
Posters: Elias fixar, detaljer när det närmar sig
Colosseum: Ska försöka, men svårt att boka, helst någon vecka innan veckan jammet är
Matinköp: frukost, snacks, ingen läsk? Kan vi beställa till lokalen? Kan beställa catering eller ica maxi grejen utanför Goto10
Organizer Tillgänglighet: Kolla med discord
Gemensam mat: Prelem. bara pizza och sushi på lördag
Marknadsföring: Discord, mail, sidor och lite mindre posters
Nästa planeringsmöte: 4e mars
Dela ut ansvarsroller:
    Huvud: Martin
    Marknadsföring: Elias
    Frukost:
    Presentation: (Henry?)


