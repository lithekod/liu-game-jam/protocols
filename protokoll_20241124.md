# Game Jam möte 2024-11-24
## Mötet öppnas 17:04
- Närvarande
    - Martin
    - Johannes (jag!)
    - Fabian
    - Elias
    - Filip
    - Flippe

## Marknadsföring
- Bara en workshop på tisdag kvar
- Allt annat fixat

## Nyckeluthämtning
- Johannes hämtar

## Lokalen
- Bra om organisers är på plats senast 16:30

## Inköping
- Handlar vid coop nära Spektrum
- Budget samma som förra gången
- Inköpslista (Martin skriver i detalj senare)
    - Frukost, lite mer än förra gången så att det räcker
    - Frukt
    - Snacks till gemensamma grejer
    - Popcorn?
- De som handlar
    - Martin
    - Fabian
    - Henry

## Saker från förrådet
- Att ta med
    - Grenkontakter, ca 10 st
    - Salta pinnar
    - Banners 
    - Skålar
    - Stickers
    - Pizzasclicers (kanske saknas från förrådet)
    - Papper & pennor
    - Te
    - (Kodapan)
    - Posters
- De som bär
    - Elias
    - Filip

## Matbeställning
- Pizza
    - Kvällsmat på fredagen
    - Anmälan i incheckningsformuläret
    - Deadline 18:30
- Sushi
    - Kvällsmat på lördagen
    - Separat formulär, skickas på lördag morgon
    - Deadline 14:00
- Betalning
    - Johannes fixar

## Rollfördelning
- Huvudansvarig: Martin
- Presentationer: Flippe
- Temagenerering: Elias
- Mottagning & uppdatera incheckningsformulär: Fabian, Johannes
- Frukostansvarig: Filip
- Matansvarig: Johannes
- Städansvarig: Fabian

## Övrigt
- Rekrytera nytt folk
    - Ha kvar möjligt att intresseanmäla i incheckningsformuläret
    - Flippe ligger in slide under presentationer
    - Alla borde göra minst en ärlig ansträngning till att rizza upp folk
        - Charmmaxxa och drick en potion of speech innan
- Rizza upp folk för att fylla i feedback-formuläret på söndagen
- Kom ihåg att kolla Spektrums checklista för städning etc

## Nästa möte
- Feedback & planering inför nästa jam
- Måndag 2024-12-09 kl 17:45

## Mötet avslutas
- Kl 17:37
- 37 min möte nytt rekord!!
