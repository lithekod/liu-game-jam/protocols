

### UTVÄRDERINGSMÖTE 2024-04-22 ###

1. utse antecknare

OK det är jag



2. Gick det bra enligt oss?

Ja men det tycker vi.
Några frågetecken på städning av papperskorgar.
Köp frukost när vi vet att det inte räcker dagen innan ju! Tillagt till lördags-checklista.



3. Svar från de som håller i Spektrum

Petiga med städning, verkar vara småsaker som i vissa fall teoretiskt sett kan vara någon annans grejs.
De verkar vilja ha oss tillbaka.



4. Nya organizers?

Flippe (Herr Eriksson) + David



5. Stats

32 pers denna gång. Det går uppåt i folk som är med, budget ser bra ut.

Snacks-detour: ställ fram på lördag så blir det nog inte lika mycket över.



6. Martin ber om ursäkt

Inga skällsord från organizers på jams framöver. Även om man sitter med sina kompisar och hänger. (främst i referens till ordet restarted)



7. Fånga nya organizers.

Kan vi hänga på Lithe Kod för att göra reklam för Liu GJ?
