# Game Jam möte 2024-09-16
## Mötet öppnas 17:51
- Närvarande
    - Martin
    - Johannes (jag!)
    - Fabian
    - Elias
    - Gustav
    - Filip
    - David (ny :D)
    - Filip från Norrköping aka Flippe (ny :D)

## Fall Game Jam
- Förslag på datum:
    - 9-10 November
        - lite tight efter tenta-p
    - 16-17 November
        - Flera av oss organisers har obligatoriskt kursmoment på fredagen
        - => ca hälften av organisers kan ej närvara kl 15-17 på fredagen :S
    - 29 November - 1 December
        - Inga schemakrockar än så länge

## Lokal
- Satsa på spektrum
- Goto 10 verkar inte vara ett hållbart alternativ längre

## Posters
- Köra på samma som förra gång
    - Få men stora posters
    - A3
- Ha några till Valla folkhögskola, några till campus Norrköping och en till ESG om vi får
    - 3 till Norrköping
    - 1 till Valla folkhögskola
    - 1 till ESG
    - 10 till Linköping
    - 3 till Colosseum
    - (På ett ungefär)
- Flippe kan printa A3 posters för Norrköping i Norrköping

## Colossuem
- Förra gången var vi där torsdagen veckan innan och tisdag samma vecka
- Lägger det på is för just nu men vi ska stå där
- Martin skapar en liten poll för vilka som kan vid ett senare datum
- Stå i Colosseum tidigt samma vecka som jammet eller veckan innan

## Workshop
- Vi har en hel termin att förbereda oss => workshop igen i år?
- Elias kan hålla i workshoppen igen
- Gustav frågar Annie om hon vill hålla en del om art & colour theory igen
    - Ingen garanti om hon kan iom att hon har jobb
- Martin kan hålla introduktion och idéskapande
- Vi fick inte så mycket respons men det var en del där förra gången
    - Ca 10 pers som gick på workshoppen gick sedan på jam:et
- Datum för workshops?
    - Beror på när game jam:et blir
    - Preliminärt Torsdag 7:e & Tisdag 12:e November
    - Dvs 2 veckor & 1 vecka innan
- Martin kollar på salsbokning av Ada
- Marknadsföring
    - Det vill vi göra

## Tillgänglighet
- Fabians bror fyller år den helgen men hon är förmodligen tillgänglig
- Martin kommer skicka enkät om tillgänglighet till on-site organisers
- Även utan on-site organisers är vi tillräckligt många organisers för att klara oss

## Matinköp
- Köra som förra gången
    - Väldigt billigt
    - Ingen jam:er verkade sakna något
    - Frukost & frukt
    - Lite snacks till presentationer
    - Se till att ställa fram snacksen så att det går åt
    - Undvika kladdiga eller "pudriga" saker
    - Kanske skippa snacks under speltestningen
    - Popcorn!
        - Billigt & trevligt
- Gemensam mat
    - Pizza på fredagen
    - Sushi på lördagen som kvällsmat
    - Jam:ers får fixa egen lunch
- Priser
    - Preliminärt samma som förra gången

## Marknadsföring
- Posters
- Colosseum
- Workshop
- LiTHe kod discord, Game jam discord, mail
    - Använda alla dem som vanligt
    - Elias skickade ut mail förra gången via mail chimp
- Ingemars föreläsningar i TSBK03
    - Typiskt bra att lägga så sent som möjligt
- Ingenjörsprofessionalismsföreläsning
    - Glest om folk
    - Ingen verkade lyssna
- Erik Berglunds föreläsningar
    - Han gillar när folk gör spel
    - Filip brukar göra marknadsföring på dessa tillfällen
    - De har bara 3 föreläsningar i början och sedan inte mycket mer :(
- Flippe är labassistens i en kurs i 3D-modellering
    - Ska försöka dra med lite folk
- Gustav ska se om han kan marknadsföra lite på ESG
    - Kanske inte rätt målgrupp
- Höra av oss till lärare på Valla folkhögskola om att få marknadsföra bortom posters
    - Martin skriver upp
- Slutsatser
    - Ja till Ingemars föreläsningar
    - Nej till Ingenjörsprofessionalism
    - Nej till Erik Berglunds spelkurs p.g.a. timing

## Rollutdelning
- Huvudansvarig: Martin
- Marknadföring:
    - Elias som ansvarig, kommunikatör och artdirector typ
    - Flippe huvudproducent av material och artdirector
- Frukost: Filip
- Städning: Fabian
- Mat: Johannes (jag är kassör numera :O)
- Presentation: 
    - Henry
    - Flippe som backup

## Från förra mötet
- Inget särskilt

## Övrigt
- Vill marknadföringsansvariga ha en intern deadline?
    - Får inte sätta upp posters för tidigt
    - Lika bra att börja med allt samtidigt
    - Intern poster deadline: 7 oktober
- Flippe poänterar att det kan vara värt att ha med en förklaring om Game Jams på postern
    - Många han pratat med har ej vetat vad game jams är
    - Räcker med en mening
- Hemsidan måste uppdateras
    - Ctrl+c Ctrl+v :nerd_emoji:
- Flippe är med i LiTHe Hack dvs motsvarighet till LiTHe kod i Norrköping (fast de får betalt!! :O)
    - De vill också hålla i game jams
    - Någon form av samarbete?
    - Organisera 2 events samtidigt för samma målgrupp
        - Nja vill hellre ha folk samlade
    - LiTHe Hack kan helt enkelt ta konceptet
        - Vi har inget monopol på LiU game jam
        - De kan få lite resurser av oss :)

## Nästa möte
- Måndag 14 Oktober kl 17:45

## Mötet avslutas 18:47
