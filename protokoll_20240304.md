
### YO ###

Möte öppet typ 17:50



### Vi har inge sal bestämt än officiellt ###

Spektrum kostar 700 i timman -4 timmar (kan vara goto10 som kostade det bli fake news'd)
Goto 10 kostar typ 8 000

Antagandet just nu är Spektrum 



### Roller ###

Marknadsföringsansvarig (inte on-site) - Elias

Städansvarig - Johannes

Tema/whateveransvarig - Johannes

Matansvarig - Fabian

Frukostansvarig - Filip


Inga snacks!
Kanske snacks, men bara vid speciella tider (tex under presentationer)? Inte en dålig idé...

Det finns ett coop typ dubbelt så stort där vid ladan. Ska vi ha en inköpsansvarig?

12-14 april är preliminärt datum då, kan folk då? Fabian och Filip kan hänga med Martin och handla troligen så länge inga kurser uppstår.


Att bestämma pilliga saker nu kan vara jobbigt för det är så långt kvar och kan ändras ändå.
Vi bör ha ett till planeringsmöte närmre!



### Colosseum ###

Kan Elias skicka ett mail om att fixa en sal eller nåt? Jag vet inte riktigt vad frågan är.



### Poster ###

På god väg :thumbs_up:. Ha en liten eye-catcher med lite info och qr-kod. Ha med tid och plats.
Kan vara klar under (om)tenta-p.
Behöver göras:
    Loggor ser skumma ut, färgsktifa/gör en annan kant på de för att få de att se ut som klistermärken?

Deadline onsdag 20/4

12-15 A3 och sätt på stora poster-väggar, spara nåra stora för Colosseum och eventet

Under 2 veckor, närmre 1 vecka för att få klistermärken. Kan vara påskproblem dock, var tidig!



### Nästa möte ###

Veckan innan (veckan innan) jammet?

Nyckeluthämtning, vilka som handlar, fixar bilar

Förra gången bekräftade vi ju nånting ett tag innan.

Om 12-14, bör vi ha möte i början av april. Måndagen är annandag påsk. Onsdagen? Tisdagen?
Tisdagen 2/4 alltså



### Övrigt? ###

Några frågetecken?

Hemsidan behöver uppdateras men behöver mer info för att göra det.

Planera text inför Discord-meddelanden. Announcement, Discord-event, Mail, Facebook?.
Martin fixar text, Elias har Facebook-inlogg

Har alla tillgång till drive-mappen? Neråtpil -> Organisera -> Stjärnmärk

Hur ligger maillistan till? De som är där är där. Dags att lägga till organizers till draft-listan.


Rekrytering inför nästa game jam måste göras. Peka ut folk, isolera dem. Jaga.



### Mötet avslutas ###

18:44

cya
