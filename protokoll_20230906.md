# Mötesprotokoll Game Jam

## 6 september 2023

## Mötet öppnades 17.32
- 6 pers närvarande (mysigt :) )
- Hej till nya organisers: Johannes (hej Martin!)
- LiTHE kod har sagt upp gamejamsanvarigeposten, istället har vi projektledare för LiU Game Jam-undergruppen (Martin)


## Fall Game Jam 2023 
- Förslag på datum:
  - LiU Game Week, 24-26 November (borde skicka ut enkät om vilka som kan)
  - 17-19 November, 24-26 November, 1-3 december


## Lokal:
- Spektrum om de är lediga
- Goto10 krävde vakter och avbokade 2 veckor innan under tidigare jam (kan ha varit för att de var nya)
- Dvs Spektrum i första hand, Goto10 i andra hand
- Vi borde på sikt försöka hålla kontakt med Goto10 eftersom sättet som Spektrum drivs på kanske kommer att ändras


## Posters:
- Elias fixar posters igen
- Skicka gärna idéer till Elias
- Posters bör sättas upp omkring 17 oktober
- Intern deadline: 1 oktober


## Colosseum:
- Kul att göra
- Boka när vi har ett datum
- Vill vara i colosseum veckan innan och veckan upp till
- Värt att kika på när relevanta föreläsningar ska vara i C1 & C2 etc
- Blir troligtvis i början av November
- Vecka 44 och framåt är relevanta, särksilt vecka 45
- För närvarande väntar vi på datum innan vi bestämmer något konkret


## Inköp:
- Fredagen som jam:et är på
- Går inte att bestämma förrän vi har datum


## Tillgänglighet:
- Svårt att säga utan datum
- Håll enkät om vilka datum som folk är tillgängliga, vilka som kan hjälpa till inför och under jam:et


## Övrigt
LiTHE kods 10-årsjubileum:
- Väldigt tyst på den punkten
- Martin undersöker


## Att nå ut till och locka folk:
- Vi har varit lite dåliga på peta på personer
- Borde hitta kanaler genom vilka vi kan nå nya personer
- Vill särskilt nå ut till Nollan
- Styfen var ett bra tillfälle för att trycka på jam:et för Nollan
- Dök upp några Nollan på LiTHE kods första meetup
- Kanske värt att synas på TikTok och Instagram (TikTok-dans i Colosseum?? :O)
- Crash:a en föreläsning? Kontakta olika föreläsare (Aseel, Erik, Ingemar Ragnemalm the OG, Ahmed, Jonas Kvarnström)
  - Martin har tomt i schemat under 
  - Klippa ihop footage från olika spel från tidigare jams?
- Få tag på mediatekniker i Norrköping
- Kontakta sektioner och föreningar
- Kontakta LiU om att skicka mail (studentnyheter)
- Incentiv för folk att ta med vänner?
- Tävling med majoritetsröst (folk säger åt sina vänner att komma och rösta :O ?)
    - Kommer inte fungera väl för detta jam (Martin har sagt till folk att det inte är en tävling smh)
- Ha lite utrustning såsom VR-headset för att locka personer? (Jesper talar om HoloLens 1 på hans jobb som ligger och skräpar ??)
- Mini Jam (workshop) om hur man gör spel (pedagogik pog :OOO banger idé)
  - Göra reklam för det i Colosseum
  - Ha posters fö det?
  - Primärt för att lära folk göra spel, sekundärt för att marknadsföra Game Jam
  - Marknadsför Game Jam och säg att inför jam:et har vi workshop
  - Gustav kan hålla i workshop om light engines
- Vifta runt namn inom industrin (ex att någon från Xbox kommer at pratar om saker, getting started with Blender)


## Nästa möte 
- Möte för detaljplanerig svårt att bestämma innan vi har datum
- Satsa på att marknadsföra på föreläsningar och ha workshop(s) och sen se om det ger utdelning
  - Ta reda på konkret vilka som kan hålla i detta
- Martin återkommer


## Mötet avslutades 18:44
