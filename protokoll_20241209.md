### REVIEW MEETING 2024-12-09 ###


## Part 1: Reviewing Fall Game Jam 2024 ##

1. Start 17:50

Hello everyone! We have new organizers. Let's introduce everyone. This one will be in english btw.



2. Taking notes

Me (Filip Berg)



3. Organizer thoughts

- Work distribution
Good balance, no one felt they had too much to do.

- Anything missing?
Malte feels Malte was missing (so true).

Organizers convincing people to become organizers.

Earlier pizza orders because now they took a long time. Maybe call like a day or week before instead of an hour, and call at the given preliminary completion time to see if they are actually done.



4. Feedback from Spektrum

A chair was broken according to Spektrum. We did not know this.

Add a slide to tell us if you (the jammer) break something.



5. New Organizers!!!!!!!

2 new BIG ones.



6. Number of jammers

40 people signed up with the form.

How many were new vs old? 11 completely new jammers!



7. Budget check

It's good. Fall Game Jam 2024 cost a bit under 10 000 kr. Pizzas were 3 468 and people paid 2 320 so it cost Lithe Kod 1 148 (I think I heard these numbers right).

We have realized that our budget is 25% larger than we thought! Yippie!



8. Responses

- 4 or 5 / 5

- People wanted more snacks

- Someone assumed sushi was for lunch

- Main takeaway: people order when they can, some people do not order sushi
Order from banchan? Discuss food options?

- Final presentations took a long time
Debate was had on if there are too many in-jokes. Be mindful, think of other people who are not in on the joke.
Possibly have a presenter wh stays up and presses next slide



## Part 2: Next Jam ##


1. Date of next Jam

Not Global Game Jam because it conflicts with stuff.

Preliminary dates:
31st jan - 2 feb
7 - 9 feb



2. Where will the Game Jam be held?

Spektrum, duh



3. Posters

Do we do them? No one mentioned posters, but they are a part of a whole.

We spent about 2000 kr for 20 posters.

If we just use A3s they will be much cheaper and we might just put them at other places than campus because they are just swamped instantly by festerister.

Possible new vector: Pamphlets. Cheap and you do get some bites. Giving out pamphlets takes effort but not that much when doing Colosseum. Though people in Colosseum and Märkesbacken want to keep to themselves and not talk to others.
Pamhplets have been voted some for - no against. Liu Gamers will send us their stuff, thanks for inspiration!



4. Colosseum

Preliminary dates: 1 time same time as the jam and 1 time the week before.



5. Workshop

Only pre Fall Game Jam so none for this one. No work shop for this one



6. Food

Breakfast + fruits works well.

Someone mentioned wanting more snacks.
Plan for snacks: Bring out for first and half-time presentations, then just leave out for people to take.

Banchan instead of sushi? Pizzas cost about 1 150 and posters 2 000 so if we buy banchan for 1 000 and reduce poster costs it is no problem.

New plan for food: pizza on Friday dinner, Banchan as Saturday dinner, sushi as Sunday lunch.



7. Present Organizers

No one knows that they will not be able to be present.



8. Marketing

Filip E is up for producing visuals for posters and such.

Liu Game Jam server, Liu Gamers discord, Mail, Posters, Facebook, Ragnemalm, Valla folkhögskola, East Sweden Game?, Colosseum, Pamphlets.



9. Date for next planning meeting

Exam period is weeks 2 and 3, so week 4. Monday 20th of january 2025.



10. Responsibility roles

- Head: Martin

- Marketing: Elias and Filip E

- Breakfast: Filip B

- Cleaning: Fabian
Some organizers felt like they walked around a lot not knowing what to do. We should have a meeting delegating all cleaning work or some area where a cleaning delegator is. We will decide on a time to have a cleaning meeting on saturday or sunday.

- Presentations: Beatrice

- Food person: Johannes for real this time (Martin stole it last time)



11. From the last meeting

Nothing here



12. Anything else?

We did a great job this jam! Yippie!



13. Meeting over

19:15
